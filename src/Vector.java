import java.util.List;

/**
 * Created by Kuba on 04.06.2018.
 */
public class Vector {
    public List<Double> Elements;

    private String label;
    public String getLabel() { return label; }
    public void setLabel(String label) { this.label = label; }

    public Vector(List<Double> elements)
    {
        this.Elements = elements;
    }

    public double dotProduct(Vector b)
    {
        double result = 0.0;
        for(int i = 0; i < Elements.size(); i++)
        {
            result += Elements.get(i) * b.Elements.get(i);
        }
        return result;
    }

    public void scalarMultiplication(double a)
    {
        Elements.stream().forEach(e -> e *= a);
    }

}
